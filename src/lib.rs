pub struct Utf8Iterator<I> {
    iter: I
}

#[derive(Debug)]
pub struct Utf8Error {
}

impl<I> From<I> for Utf8Iterator<I> {
    fn from(value: I) -> Self {
        Self { iter: value }
    }
}

impl<I> Utf8Iterator<I>
where
    I: Iterator<Item = u8>,
{
    fn get_row(&mut self, amount: usize) -> Result<u32, Utf8Error> {
        let mut result = 0;
        for _ in 0..amount {
            let v = self.iter.next().ok_or(Utf8Error{})?;
            if v & 0xC0 != 0x80 {
                return Err(Utf8Error {});
            }
            result = (result << 6) | ((v & 0x3F) as u32);
        }
        Ok(result)
    }

    fn get(&mut self, first: u8) -> Result<u32, Utf8Error> {
        match first {
            0x00..=0x7F => Ok(first as u32),
            0xC0..=0xDF => Ok((((first & 0x1F) as u32) << 6)  | self.get_row(1)?),
            0xE0..=0xEF => Ok((((first & 0x0F) as u32) << 12) | self.get_row(2)?),
            0xF0..=0xF7 => Ok((((first & 0x07) as u32) << 18) | self.get_row(3)?),
            _ => Err(Utf8Error {}),
        }
    }
}

impl<I> Iterator for Utf8Iterator<I>
where
    I: Iterator<Item = u8>,
{
    type Item = Result<char, Utf8Error>;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|x| {
            let result = self.get(x)?;
            let result = char::from_u32(result).ok_or(Utf8Error{})?;
            Ok(result)
        })
    }
}

#[test]
fn general_test() {
    let sample = "Кириллица";
    let mut iter = Utf8Iterator::from(sample.bytes());
    assert_eq!(iter.next().unwrap().unwrap(), 'К');
    assert_eq!(iter.next().unwrap().unwrap(), 'и');
    assert_eq!(iter.next().unwrap().unwrap(), 'р');
    assert_eq!(iter.next().unwrap().unwrap(), 'и');
    assert_eq!(iter.next().unwrap().unwrap(), 'л');
    assert_eq!(iter.next().unwrap().unwrap(), 'л');
    assert_eq!(iter.next().unwrap().unwrap(), 'и');
    assert_eq!(iter.next().unwrap().unwrap(), 'ц');
    assert_eq!(iter.next().unwrap().unwrap(), 'а');
    assert!(iter.next().is_none());
}
